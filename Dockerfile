FROM openjdk:11
VOLUME /tmp
EXPOSE 9999
ADD build/libs/*.jar authorization.jar
ENV JAVA_OPTS = "-Xmx1536m"
ENTRYPOINT ["sh","-c", "java -Xmx1536m -jar authorization.jar"]