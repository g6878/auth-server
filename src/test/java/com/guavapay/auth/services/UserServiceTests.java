package com.guavapay.auth.services;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.guavapay.auth.repository.AuthorityRepository;
import com.guavapay.auth.repository.UserRepository;
import com.guavapay.auth.services.serviceImpls.UserServiceImpl;
import com.guavapay.auth.utils.Utils;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class UserServiceTests {

    @Mock
    UserRepository userRepository;

    @Mock
    AuthorityRepository authorityRepository;

    @Mock
    Utils utils;

    @InjectMocks
    UserServiceImpl userService;

/*    @Test
    public void checkIfCreateUsersIsAlwaysOk() {
        List<AuthServerUserImportDto> users = new ArrayList<>();
        userService.createUsers(users);
        Mockito.verify(userRepository, Mockito.times(1)).saveAll(any());
    }

    /*@Test
    public void checkIfAuthorityNotAddedThrowsAuthorityNotAddedException() {
        Mockito.when(authorityRepository.findById(anyString())).thenReturn(Optional.empty());
        *//*assertThrows(AuthorityNotAddedException.class, () -> userService.createUsers(new ArrayList<>()));*//*
        Mockito.verify(userRepository, Mockito.times(0)).saveAll(any());
    }*/

    /*@Test
    public void checkIfRegisterUserIsEverythingOk() {
        Mockito.when(userRepository.findByEmail(anyString())).thenReturn(new User());
        RegisterUserDto dto = new RegisterUserDto();
        userService.registerUser(dto);
        Mockito.verify(userRepository, Mockito.times(1)).findByEmail(anyString());
    }*/

/*    @Test
    void checkIfConfirmTokenIsEverythingOk() {
        Mockito.when(userRepository.findByActivationToken(anyString())).thenReturn(new User());
        utils.hasTokenExpired("false");
        userService.confirmToken(anyString());
    }

    /*@Test
    void checkIfResetPasswordNotificationIsEverythingOk() {
        Mockito.when(userRepository.findByEmail(anyString())).thenReturn(new User());
        userRepository.save(new User());
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        emailSenderService.sendEmail(mailMessage);
    }*/

/*    @Test
    void resetPassword() {
    }

    /*@Test
    public void checkIfChangePasswordIsEverythingOk() {
        Mockito.when(userRepository.findByUsernameCaseInsensitive(anyString())).thenReturn(new User());
        userRepository.findByUsernameCaseInsensitive(anyString());
        PasswordResetDto passwordResetDto = new PasswordResetDto();
        userService.resetPassword(passwordResetDto);
        Mockito.verify(userRepository, Mockito.times(1)).save(any());
    }*/
}