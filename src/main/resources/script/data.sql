INSERT INTO users (id,username, email, password, activated,blocked) VALUES (1,'csmuxa@gmail.com', 'csmuxa@gmail.com', '$2a$10$RsHYWePDGcrcTumFe2d8V.fZL9ePgcGnzBHMrx8jkcJ3swaKqnCWG', true , false);
INSERT INTO users (id,username, email, password, activated,blocked) VALUES (2,'csxado@gmail.com', 'csxado@gmail.com', '$2a$10$RsHYWePDGcrcTumFe2d8V.fZL9ePgcGnzBHMrx8jkcJ3swaKqnCWG', true, false);

INSERT INTO authority (name) VALUES ('ROLE_USER');
INSERT INTO authority (name) VALUES ('ROLE_ADMIN');
INSERT INTO authority (name) VALUES ('ROLE_COURIER');
-->User of Muxammed
INSERT INTO user_authority (user_id, authority) VALUES (1, 'ROLE_USER');
INSERT INTO user_authority (user_id, authority) VALUES (1, 'ROLE_ADMIN');
-->User of Rasim
INSERT INTO user_authority (user_id, authority) VALUES (2, 'ROLE_USER');
INSERT INTO user_authority (user_id, authority) VALUES (2, 'ROLE_ADMIN');

INSERT INTO oauth_client_details (client_id, client_secret, scope, authorized_grant_types, access_token_validity, additional_information)
VALUES ('account-service', '$2a$10$rAuhXTa1uAtRyFaGn1yPrOr7iG9Airb3gtleLmYKOT9l6EcXSy6u2', 'pc,android,ios', 'authorization_code,password,refresh_token,implicit', '900', '{}');
INSERT INTO oauth_client_details (client_id, client_secret, scope, authorized_grant_types, access_token_validity, additional_information)
VALUES ('customer-service', '$2a$10$rAuhXTa1uAtRyFaGn1yPrOr7iG9Airb3gtleLmYKOT9l6EcXSy6u2', 'pc,android,ios', 'authorization_code,password,refresh_token,implicit', '900', '{}');
