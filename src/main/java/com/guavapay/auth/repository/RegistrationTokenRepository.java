package com.guavapay.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.guavapay.auth.entities.RegistrationToken;

public interface RegistrationTokenRepository extends JpaRepository<RegistrationToken, Long> {

    RegistrationToken findByRegistrationToken(String token);

}
