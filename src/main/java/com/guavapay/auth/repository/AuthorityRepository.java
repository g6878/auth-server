package com.guavapay.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.guavapay.auth.entities.Authority;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority, String> {

    Optional<Authority> findByName(String name);

}
