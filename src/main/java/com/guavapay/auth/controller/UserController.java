package com.guavapay.auth.controller;


import com.guavapay.auth.dtos.ChangePasswordDto;
import com.guavapay.auth.dtos.CourierDto;
import com.guavapay.auth.dtos.PasswordResetDto;
import com.guavapay.auth.dtos.RegisterUserDto;
import com.guavapay.auth.entities.Authority;
import com.guavapay.auth.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping(path = "/account")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/registration")
    public void registerUserAccount(@Valid @RequestBody RegisterUserDto registerUserDto) {
        userService.registerUser(registerUserDto);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/courier")
    public void createCourier(@Valid @RequestBody CourierDto courierDto) {
        userService.createCourier(courierDto);
    }

    @GetMapping(path = "/registration/confirm")
    public void confirmToken(@RequestParam("token") String registrationToken) {
        userService.confirmToken(registrationToken);
    }

    @PostMapping("/password-reset/{email}")
    public void registerUserAccount(@PathVariable String email) {
        userService.resetPasswordNotification(email);
    }

    @PostMapping(path = "/password-reset/reset")
    public void confirmToken(@Valid @RequestBody PasswordResetDto passwordResetDto) {
        userService.resetPassword(passwordResetDto);
    }

    @PostMapping(path = "/change-password")
    public void changePassword(@Valid @RequestBody ChangePasswordDto changePasswordDto) {
        userService.changePassword(changePasswordDto);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(path = "/block/{email}")
    public void blockUser(@PathVariable String email) {
        userService.blockUser(email);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(path = "/unblock/{email}")
    public void unblockUser(@PathVariable String email) {
        userService.unblockUser(email);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(path = "/email/role")
    public Map<String,String> getEmailsAndRoles(){
        return userService.getEmailsAndRoles();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(path = "/roles/{email}")
    public Set<Authority> getUserAuthorities(@PathVariable String email) {
        return userService.getUserAuthorities(email);
    }
}
