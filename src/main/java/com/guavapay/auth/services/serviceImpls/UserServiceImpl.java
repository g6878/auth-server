package com.guavapay.auth.services.serviceImpls;

import com.guavapay.auth.dtos.ChangePasswordDto;
import com.guavapay.auth.dtos.CourierDto;
import com.guavapay.auth.dtos.PasswordResetDto;
import com.guavapay.auth.dtos.RegisterUserDto;
import com.guavapay.auth.entities.Authority;
import com.guavapay.auth.entities.User;
import com.guavapay.auth.exceptions.*;
import com.guavapay.auth.rabbit.RabbitProducer;
import com.guavapay.auth.repository.AuthorityRepository;
import com.guavapay.auth.repository.UserRepository;
import com.guavapay.auth.services.UserService;
import com.guavapay.auth.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static com.guavapay.auth.security.Authorities.ROLE_COURIER;
import static com.guavapay.auth.security.Authorities.ROLE_USER;


@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;

    private final PasswordEncoder passwordEncoder;
    private final Utils utils;

    private final RabbitProducer rabbitProducer;


    @Override
    public void registerUser(RegisterUserDto dto) {
        String email = dto.getEmail();
        User user = userRepository.findByEmail(email).orElse(new User());
        if (user.isActivated()) throw new UserAlreadyExistException();
        Authority authority = authorityRepository.findByName(String.valueOf(ROLE_USER)).orElseThrow(AuthorityNotAddedException::new);
        Set<Authority> authorities = new HashSet<>();
        authorities.add(authority);

        user
                .setBlocked(false)
                .setActivated(false)
                .setEmail(email)
                .setUsername(email)
                .setAuthorities(authorities)
                .setPassword(passwordEncoder.encode(dto.getPassword()))
                .setActivationToken(utils.generateEmailVerificationToken(user.getEmail()));
        userRepository.save(user);
        userRepository.flush();

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setSubject("Registration submit");
        mailMessage.setFrom("tenderghostjanitor@gmail.com");
        mailMessage.setText("Click to submit registration: "
                + "http://localhost:9999/account/registration/confirm?token=" + user.getActivationToken());
        rabbitProducer.send(mailMessage);

    }

    @Override
    public void createCourier(CourierDto courierDto) {

        User user = userRepository.findByEmail(courierDto.getEmail()).orElse(new User());
        if (user.isActivated()) throw new UserAlreadyExistException();

        Authority authority = authorityRepository.findByName(String.valueOf(ROLE_COURIER)).orElseThrow(AuthorityNotAddedException::new);

        Set<Authority> authorities = new HashSet<>();
        authorities.add(authority);

        user.setUsername(courierDto.getEmail())
                .setEmail(courierDto.getEmail())
                .setPassword(passwordEncoder.encode(Arrays.toString(courierDto.getPassword())))
                .setActivated(true)
                .setBlocked(false)
                .setAuthorities(authorities);

        userRepository.save(user);
    }

    @Override
    public void confirmToken(String activationToken) {
        if (utils.hasTokenExpired(activationToken)) throw new TokenExpiredException();

        User user = userRepository.findByActivationToken(activationToken);
        if (user == null) throw new UserWithThisTokenNotFoundException();

        user.setActivated(true);
        userRepository.save(user);
    }

    @Override
    public void resetPasswordNotification(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(UserNotFoundInSystemException::new);
        if (user == null) throw new UserNotFoundInSystemException();
        if (user.isBlocked()) throw new UserBlockedException();

        user.setResetPasswordToken(utils.generatePasswordResetToken(email));
        userRepository.save(user);
        userRepository.flush();

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setSubject("Registration submit");
        mailMessage.setFrom("tenderghostjanitor@gmail.com");
        mailMessage.setText("Click to submit password change: "
                + "http://localhost:9999/account/password-reset/reset?token=" + user.getResetPasswordToken());
        rabbitProducer.send(mailMessage);
    }

    @Override
    public void resetPassword(PasswordResetDto passwordResetDto) {
        if (utils.hasTokenExpired(passwordResetDto.getPasswordResetToken()))
            throw new ResetTokenExpiredException();
        User user = userRepository.findByResetPasswordToken(passwordResetDto.getPasswordResetToken());
        if (user == null) throw new UserNotFoundInSystemException();
        if (!user.isActivated()) throw new UserIsNotActiveException();
        if (user.isBlocked()) throw new UserBlockedException();
        user.setPassword(passwordEncoder.encode(passwordResetDto.getPassword()));
        user.setResetPasswordToken(null);
        userRepository.save(user);
    }

    @Override
    public void changePassword(ChangePasswordDto changePasswordDto) {
        Authentication principal = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByUsernameCaseInsensitive(principal.getName());
        if (user == null) throw new UserNotFoundInSystemException();
        if (user.isBlocked()) throw new UserBlockedException();
        if (!passwordEncoder.matches(changePasswordDto.getOldPassword(), user.getPassword()))
            throw new PasswordIncorrectException();
        user.setPassword(passwordEncoder.encode(changePasswordDto.getNewPassword()));
        userRepository.save(user);
    }

    @Override
    public void blockUser(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(UserNotFoundInSystemException::new);
        if (user == null) throw new CouldNotBlockEmployeeException();
        user.setBlocked(true);
        userRepository.save(user);
    }

    @Override
    public void unblockUser(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(UserNotFoundInSystemException::new);
        if (user == null) throw new CouldNotUnblockEmployeeException();
        user.setBlocked(false);
        userRepository.save(user);
    }

    @Override
    public Set<Authority> getUserAuthorities(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(UserNotFoundInSystemException::new);
        return user.getAuthorities();
    }

    @Override
    public Map<String, String> getEmailsAndRoles() {
        List<User> users = userRepository.findAll();

        return users.stream().collect(Collectors.toMap(User::getEmail, u -> u.getAuthorities().stream().map(Authority::getName).collect(Collectors.joining(","))));
    }
}
