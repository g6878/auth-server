package com.guavapay.auth.services;

import com.guavapay.auth.dtos.ChangePasswordDto;
import com.guavapay.auth.dtos.CourierDto;
import com.guavapay.auth.dtos.PasswordResetDto;
import com.guavapay.auth.dtos.RegisterUserDto;
import com.guavapay.auth.entities.Authority;

import java.util.Map;
import java.util.Set;

public interface UserService {

    void registerUser(RegisterUserDto dto);

    void createCourier(CourierDto courierDto);

    void confirmToken(String registrationToken);

    void resetPasswordNotification(String email);

    void resetPassword(PasswordResetDto passwordResetDto);

    void changePassword(ChangePasswordDto changePasswordDto);

    void blockUser(String email);

    void unblockUser(String email);

    Set<Authority> getUserAuthorities(String email);

    Map<String, String> getEmailsAndRoles();
}
