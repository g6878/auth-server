package com.guavapay.auth.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User {

	@Id
	@SequenceGenerator(name = "user_id_seq",
			sequenceName = "user_id_seq",
			allocationSize = 1)
	@GeneratedValue(generator = "user_id_seq", strategy = GenerationType.SEQUENCE)
	private long id;

	@Column(name = "username", updatable = false, nullable = false)
	@Size(max = 50)
	private String username;

	@Column(name = "password")
	@Size(max = 500)
	private String password;

	@Column(name = "email", nullable = false)
	@Email
	@Size(max = 50)
	private String email;

	@Column(name = "activated", nullable = false)
	private boolean activated;

	@Column(name = "activation_key")
	@Size(max = 500)
	private String activationToken;

	@Column(name = "reset_password_token")
	@Size(max = 500)
	private String resetPasswordToken;

	@Column(name = "blocked", nullable = false)
	private boolean blocked;

	@ManyToMany
	@JoinTable(name = "user_authority",
			joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "authority"))
	private Set<Authority> authorities;

	public User() {
		super();
		this.activated=false;
		this.blocked=false;
	}

}
