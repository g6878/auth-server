package com.guavapay.auth.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
public class RegistrationToken {

    @Id
    @SequenceGenerator(name = "registration_token_id_seq",
            sequenceName = "registration_token_id_seq",
            allocationSize = 1)
    @GeneratedValue(generator = "registration_token_id_seq", strategy = GenerationType.SEQUENCE)
    private long id;


    @Column(name = "registration_token", nullable = false)
    private String registrationToken;

    @Column(name = "created_date", updatable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    @CreationTimestamp
    private Date createdDate;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    public RegistrationToken() {
        registrationToken = UUID.randomUUID().toString();
    }
}
