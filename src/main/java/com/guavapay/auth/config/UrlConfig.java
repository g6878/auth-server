package com.guavapay.auth.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class UrlConfig {
    @Value("${userActivationTokenExpireTime}")
    private long userActivationTokenExpireTime;
    @Value("${userPasswordResetTokenExpireTime}")
    private long userPasswordResetTokenExpireTime;
    @Value("${tokenSecret}")
    private String tokenSecret;
    @Value("${admin.users.mails}")
    private String adminUsers;
}
