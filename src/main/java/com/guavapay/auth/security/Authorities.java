package com.guavapay.auth.security;

public enum Authorities {

    ROLE_COURIER,
    ROLE_USER,
    ROLE_ADMIN;
	
}
