package com.guavapay.auth.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public enum ExceptionMessages {

    TOKEN_HAS_EXPIRED("token.has.expired"),
    USER_LINKED_TO_THIS_TOKEN_NOT_FOUND("user.linked.to.this.token.not.found"),
    USER_NOT_ACTIVATED("user.not.activated"),
    USER_ALREADY_EXISTS("user.exists"),
    USER_NOT_FOUND_IN_SYSTEM("user.not.found.in.system"),
    USER_NOT_ACTIVE("user.inactive"),
    AUTHORITY_NOT_ADDED("authority.not.added"),
    PASSWORD_IS_INCORRECT("password.is.incorrect"),
    USER_BLOCKED("account.is.blocked"),
    COULD_NOT_BLOCK_EMPLOYEE("could.not.block.employee"),
    COULD_NOT_UNBLOCK_EMPLOYEE("could.not.unblock.employee"),
    INTERNAL_SERVER_ERROR("Internal server error"),
    NO_RECORD_FOUND("Record with provided id is not found"),
    AUTHENTICATION_FAILED("Authentication failed"),
    COULD_NOT_UPDATE_RECORD("Could not update record"),
    COULD_NOT_DELETE_RECORD("Could not delete record"),
    TABLE_NOT_EMPTY("Import error.Table not empty"),
    RECORD_ALREADY_EXISTS("Record already exists");


    @Getter
    @Setter
    private String errorMessage;
}
