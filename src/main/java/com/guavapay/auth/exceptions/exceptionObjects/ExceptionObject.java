package com.guavapay.auth.exceptions.exceptionObjects;



import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionObject {

    private boolean success;

    private List<String> errors;

}
