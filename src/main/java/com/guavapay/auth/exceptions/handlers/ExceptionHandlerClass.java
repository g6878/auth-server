package com.guavapay.auth.exceptions.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.guavapay.auth.exceptions.*;
import com.guavapay.auth.exceptions.exceptionObjects.ExceptionObject;
import com.guavapay.auth.models.ExceptionMessage;
import com.guavapay.auth.models.ExceptionMessages;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionHandlerClass extends ResponseEntityExceptionHandler {

    @Autowired
    MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> validationList = ex.getBindingResult().getFieldErrors().stream().map(fieldError->fieldError.getDefaultMessage()).collect(Collectors.toList());

        return new ResponseEntity<>(new ExceptionObject(false,validationList), status);
    }

    @ExceptionHandler(UserNotActivatedException.class)
    protected ResponseEntity<ExceptionMessage> handleUserNotActivated(Locale locale) {

        return new ResponseEntity<>(new ExceptionMessage(messageSource.getMessage(ExceptionMessages.
                USER_NOT_ACTIVATED.getErrorMessage(), new Object[]{}, locale), new Date().toString()), HttpStatus.ALREADY_REPORTED);
    }

    @ExceptionHandler(UserAlreadyExistException.class)
    protected ResponseEntity<ExceptionMessage> handleUserUserAlreadyExists(Locale locale) {

        return new ResponseEntity<>(new ExceptionMessage(messageSource.getMessage(ExceptionMessages.
                USER_NOT_ACTIVATED.getErrorMessage(), new Object[]{}, locale), new Date().toString()), HttpStatus.ALREADY_REPORTED);
    }

    @ExceptionHandler(TokenExpiredException.class)
    protected ResponseEntity<ExceptionMessage> handleTokenExpiration(Locale locale) {

        return new ResponseEntity<>(new ExceptionMessage(messageSource.getMessage(ExceptionMessages.
                TOKEN_HAS_EXPIRED.getErrorMessage(), new Object[]{}, locale), new Date().toString()), HttpStatus.ALREADY_REPORTED);
    }
    @ExceptionHandler(UserBlockedException.class)
    protected ResponseEntity<ExceptionMessage> handleUserBlockedException(Locale locale) {

        return new ResponseEntity<>(new ExceptionMessage(messageSource.getMessage(ExceptionMessages.
                USER_BLOCKED.getErrorMessage(), new Object[]{}, locale), new Date().toString()), HttpStatus.ALREADY_REPORTED);
    }

    @ExceptionHandler(UserWithThisTokenNotFoundException.class)
    protected ResponseEntity<ExceptionMessage> handleUserWithNoToken(Locale locale) {

        return new ResponseEntity<>(new ExceptionMessage(messageSource.getMessage(ExceptionMessages.
                USER_LINKED_TO_THIS_TOKEN_NOT_FOUND.getErrorMessage(), new Object[]{}, locale), new Date().toString()), HttpStatus.ALREADY_REPORTED);
    }

    @ExceptionHandler(UserNotFoundInSystemException.class)
    protected ResponseEntity<ExceptionMessage> handleUserNotFound(Locale locale) {

        return new ResponseEntity<>(new ExceptionMessage(messageSource.getMessage(ExceptionMessages.
                USER_NOT_FOUND_IN_SYSTEM.getErrorMessage(), new Object[]{}, locale), new Date().toString()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UserIsNotActiveException.class)
    protected ResponseEntity<ExceptionMessage> handleUserNotActive(Locale locale) {

        return new ResponseEntity<>(new ExceptionMessage(messageSource.getMessage(ExceptionMessages.
                USER_NOT_ACTIVE.getErrorMessage(), new Object[]{}, locale), new Date().toString()), HttpStatus.ALREADY_REPORTED);
    }

    @ExceptionHandler(AuthorityNotAddedException.class)
    protected ResponseEntity<ExceptionMessage> handleAuthorityAdding(Locale locale) {

        return new ResponseEntity<>(new ExceptionMessage(messageSource.getMessage(ExceptionMessages.
                AUTHORITY_NOT_ADDED.getErrorMessage(), new Object[]{}, locale), new Date().toString()), HttpStatus.ALREADY_REPORTED);
    }

    @ExceptionHandler(PasswordIncorrectException.class)
    protected ResponseEntity<ExceptionMessage> handlePasswordIncorrect(Locale locale) {

        return new ResponseEntity<>(new ExceptionMessage(messageSource.getMessage(ExceptionMessages.
                PASSWORD_IS_INCORRECT.getErrorMessage(), new Object[]{}, locale), new Date().toString()), HttpStatus.ALREADY_REPORTED);
    }

    @ExceptionHandler(CouldNotBlockEmployeeException.class)
    protected ResponseEntity<ExceptionMessage> handleBlockEmployeeException(Locale locale) {
        return new ResponseEntity<>(new ExceptionMessage(messageSource.getMessage(
                ExceptionMessages.COULD_NOT_BLOCK_EMPLOYEE.getErrorMessage(), new Object[]{}, locale),
                new Date().toString()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CouldNotUnblockEmployeeException.class)
    protected ResponseEntity<ExceptionMessage> handleUnblockEmployeeException(Locale locale) {
        return new ResponseEntity<>(new ExceptionMessage(
                messageSource.getMessage(ExceptionMessages.COULD_NOT_UNBLOCK_EMPLOYEE.getErrorMessage(), new Object[]{}, locale),
                new Date().toString()), HttpStatus.NOT_FOUND);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler(DataIntegrityViolationException.class)
    public ExceptionMessage handleDataIntegrityViolationException(DataIntegrityViolationException ex) {
        return new ExceptionMessage(ex.getMostSpecificCause().getMessage(), new Date().toString());
    }

}
