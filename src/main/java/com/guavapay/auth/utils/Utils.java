package com.guavapay.auth.utils;

import com.guavapay.auth.config.UrlConfig;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;


@Component
@RequiredArgsConstructor
public class Utils {

    private final UrlConfig urlConfig;

    public boolean hasTokenExpired(String token) {

        boolean returnValue;
        try {
            String encodedSecret = Base64.getEncoder().encodeToString(urlConfig.getTokenSecret().getBytes(StandardCharsets.UTF_8));
            Claims claims = Jwts.parser().setSigningKey(encodedSecret).parseClaimsJws(token).getBody();

            Date tokenExpirationDate = claims.getExpiration();
            Date todayDate = new Date();

            returnValue = tokenExpirationDate.before(todayDate);
        } catch (ExpiredJwtException e) {
            returnValue = true;
        }

        return returnValue;

    }

    public String generateEmailVerificationToken(String userId) {
        System.out.println(urlConfig.getTokenSecret());
        System.out.println(urlConfig.getUserActivationTokenExpireTime());
        String encodedSecret = Base64.getEncoder().encodeToString(urlConfig.getTokenSecret().getBytes());
        return Jwts.builder().setSubject(userId).setExpiration(new Date(System.currentTimeMillis() + urlConfig.getUserActivationTokenExpireTime())).signWith(SignatureAlgorithm.HS512, encodedSecret).compact();
    }

    public String generatePasswordResetToken(String userId) {
        String encodedSecret = Base64.getEncoder().encodeToString(urlConfig.getTokenSecret().getBytes());
        return Jwts.builder().setSubject(userId).setExpiration(new Date(System.currentTimeMillis() + urlConfig.getUserPasswordResetTokenExpireTime())).signWith(SignatureAlgorithm.HS512, encodedSecret).compact();
    }

}