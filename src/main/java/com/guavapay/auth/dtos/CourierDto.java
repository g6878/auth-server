package com.guavapay.auth.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourierDto {
    private String email;
    private char[] password;
    private char[] confirmPassword;
}
