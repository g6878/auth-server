package com.guavapay.auth.dtos;

import lombok.Getter;
import lombok.Setter;
import com.guavapay.auth.annotations.FieldMatchAnnotation.FieldMatch;

@Getter
@Setter
@FieldMatch(first = "newPassword", second = "confirmPassword", message = "The password fields must match")
public class RegisterUserDto {

    private String email;

    private String password;

    private String confirmPassword;
}
