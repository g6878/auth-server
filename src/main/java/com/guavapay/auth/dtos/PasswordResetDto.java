package com.guavapay.auth.dtos;

import lombok.Getter;
import lombok.Setter;
import com.guavapay.auth.annotations.FieldMatchAnnotation.FieldMatch;

@Getter
@Setter
@FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match")
public class PasswordResetDto {

    private String passwordResetToken;

    private String password;

    private String confirmPassword;
}
