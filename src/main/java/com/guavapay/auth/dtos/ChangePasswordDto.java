package com.guavapay.auth.dtos;


import lombok.Getter;
import lombok.Setter;
import com.guavapay.auth.annotations.FieldMatchAnnotation.FieldMatch;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@FieldMatch(first = "newPassword", second = "confirmPassword", message = "The password fields must match")
public class ChangePasswordDto {

    @NotBlank
    private String oldPassword;
    @NotBlank
    private String newPassword;
    @NotBlank
    private String confirmPassword;
}
