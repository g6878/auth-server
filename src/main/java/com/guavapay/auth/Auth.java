package com.guavapay.auth;

import java.security.Principal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@RestController
@EnableEurekaClient
@EnableFeignClients
@EnableSwagger2
public class Auth {

	public static void main(String[] args) {
		SpringApplication.run(Auth.class, args);
	}

	@GetMapping("/user")
	public Principal user(Principal user) {
		return user;
	}
}
